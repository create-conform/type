/////////////////////////////////////////////////////////////////////////////////////////////
//
// type
//
//    Library for working with primitives.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var type;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Type Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Type() {
    var self = this;

    this.TYPE_UNKNOWN = "unknown";
    this.TYPE_STRING = "string";
    this.TYPE_NUMBER = "number";
    this.TYPE_BOOLEAN = "boolean";
    this.TYPE_ARRAY = "array";
    this.TYPE_OBJECT = "object";
    this.TYPE_FUNCTION = "function";

    this.getType = function(obj) {
        if(self.isString(obj)) {
            return self.TYPE_STRING;
        }
        if(self.isBoolean(obj)) {
            return self.TYPE_BOOLEAN;
        }
        if(self.isNumber(obj)) {
            return self.TYPE_NUMBER;
        }
        if(self.isArray(obj)) {
            return self.TYPE_ARRAY;
        }
        if(self.isObject(obj)) {
            return self.TYPE_OBJECT;
        }
        if(self.isFunction(obj)) {
            return self.TYPE_FUNCTION;
        }
        return self.TYPE_UNKNOWN;
    };
    /**
     * Tests if an object is an array.
     * @param {object} obj - The object to test.
     * @returns Returns true if the given object is an array.
     **/
    this.isArray = function (obj) {
        return Object.prototype.toString.call(obj) === "[object Array]";
    };
    this.isString = function (obj) {
        return Object.prototype.toString.call(obj) === "[object String]";
    };
    this.isObject = function (obj) {
        return Object.prototype.toString.call(obj) === "[object Object]";
    };
    this.isNumber = function (obj) {
        return obj != null && obj !== true && obj !== false && !isNaN(obj);
    };
    this.isBoolean = function (obj) {
        return obj != null && obj.constructor === Boolean;
    };
    this.isFunction = function (obj) {
        var getType = {};
        return obj && getType.toString.call(obj) === "[object Function]";
    };
    this.isPrimitive = function (obj) {
        return self.isString(obj) || self.isNumber(obj) || self.isBoolean(obj);
    };
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Singleton Instance
//
/////////////////////////////////////////////////////////////////////////////////////////////
type = new Type();

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = type;